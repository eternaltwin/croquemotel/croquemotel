module.exports = {
  projects: [
    {
      root: "./app/client",
      package: "../package.json",
      tsconfig: "./tsconfig.json"
    }
  ]
};
