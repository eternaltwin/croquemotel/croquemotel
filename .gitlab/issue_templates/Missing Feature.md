<!-- Use this template to describe a missing feature of the original game -->

## Description

<!-- Describe the missing feature -->

## Screenshots

<!-- Add screenshots showing the feature (if relevant) -->

/label ~"type::initial"
