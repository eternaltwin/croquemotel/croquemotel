# CroqueMotel

## Resources

Original assets came from [CroqueMotel-extra](https://gitlab.com/eternal-twin/croquemotel/croquemotel-extra) repository.

Images sprites are generated with [TexturePacker](https://www.codeandweb.com/texturepacker) (`sprites.tps` file).

Generated sprite file is inside `app/client/static`.

## Tools

To manipulate images you can use [ImageMagick CLI](https://imagemagick.org/script/command-line-processing.php).  

### Crop multiple images

```
magick mogrify -path result -format png -crop 35x69+0+72 +repage *.png
```

This command will crop all `.png` images to new `.png` files inside `result` folder with a 35x69 size and 0x72 pixels offset.

`result` folder must exist before doing this operation.
