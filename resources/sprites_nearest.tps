<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>5.5.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>pixijs4</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">PngQuantLow</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>1</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>1</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../app/client/static/sprites_nearest.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0,0</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">sprites_nearest/awning.png</key>
            <key type="filename">sprites_nearest/awning_bg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,23,42,46</rect>
                <key>scale9Paddings</key>
                <rect>21,23,42,46</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/bedroom_1.png</key>
            <key type="filename">sprites_nearest/bedroom_2.png</key>
            <key type="filename">sprites_nearest/bedroom_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,18,25,35</rect>
                <key>scale9Paddings</key>
                <rect>13,18,25,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/bedroom_door_0.png</key>
            <key type="filename">sprites_nearest/bedroom_door_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,13,15,27</rect>
                <key>scale9Paddings</key>
                <rect>8,13,15,27</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/bg_building_1.png</key>
            <key type="filename">sprites_nearest/bg_building_2.png</key>
            <key type="filename">sprites_nearest/bg_building_3.png</key>
            <key type="filename">sprites_nearest/bg_building_4.png</key>
            <key type="filename">sprites_nearest/bg_building_5.png</key>
            <key type="filename">sprites_nearest/bg_building_6.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>48,82,97,163</rect>
                <key>scale9Paddings</key>
                <rect>48,82,97,163</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/bg_day.png</key>
            <key type="filename">sprites_nearest/bg_night.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>163,110,325,221</rect>
                <key>scale9Paddings</key>
                <rect>163,110,325,221</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/clock.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,8,31,15</rect>
                <key>scale9Paddings</key>
                <rect>16,8,31,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/cloud_1.png</key>
            <key type="filename">sprites_nearest/cloud_2.png</key>
            <key type="filename">sprites_nearest/cloud_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>31,16,61,31</rect>
                <key>scale9Paddings</key>
                <rect>31,16,61,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/cursor_drag.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,13,27,25</rect>
                <key>scale9Paddings</key>
                <rect>13,13,27,25</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/fence.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,8,9</rect>
                <key>scale9Paddings</key>
                <rect>4,4,8,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/floor.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,2,21,3</rect>
                <key>scale9Paddings</key>
                <rect>11,2,21,3</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/ground.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>20,16,40,31</rect>
                <key>scale9Paddings</key>
                <rect>20,16,40,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/hotel_door.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,20,19,39</rect>
                <key>scale9Paddings</key>
                <rect>9,20,19,39</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/hotel_star.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,5,9,9</rect>
                <key>scale9Paddings</key>
                <rect>5,5,9,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/lab_bar.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,2,32,3</rect>
                <key>scale9Paddings</key>
                <rect>16,2,32,3</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/lab_door.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,16,35,31</rect>
                <key>scale9Paddings</key>
                <rect>18,16,35,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/lab_flare.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>11,11,21,21</rect>
                <key>scale9Paddings</key>
                <rect>11,11,21,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/lab_light.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,7,7</rect>
                <key>scale9Paddings</key>
                <rect>4,4,7,7</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/lab_light/1.png</key>
            <key type="filename">sprites_nearest/lab_light/10.png</key>
            <key type="filename">sprites_nearest/lab_light/11.png</key>
            <key type="filename">sprites_nearest/lab_light/12.png</key>
            <key type="filename">sprites_nearest/lab_light/13.png</key>
            <key type="filename">sprites_nearest/lab_light/14.png</key>
            <key type="filename">sprites_nearest/lab_light/15.png</key>
            <key type="filename">sprites_nearest/lab_light/16.png</key>
            <key type="filename">sprites_nearest/lab_light/17.png</key>
            <key type="filename">sprites_nearest/lab_light/18.png</key>
            <key type="filename">sprites_nearest/lab_light/19.png</key>
            <key type="filename">sprites_nearest/lab_light/2.png</key>
            <key type="filename">sprites_nearest/lab_light/20.png</key>
            <key type="filename">sprites_nearest/lab_light/21.png</key>
            <key type="filename">sprites_nearest/lab_light/22.png</key>
            <key type="filename">sprites_nearest/lab_light/23.png</key>
            <key type="filename">sprites_nearest/lab_light/24.png</key>
            <key type="filename">sprites_nearest/lab_light/25.png</key>
            <key type="filename">sprites_nearest/lab_light/26.png</key>
            <key type="filename">sprites_nearest/lab_light/27.png</key>
            <key type="filename">sprites_nearest/lab_light/28.png</key>
            <key type="filename">sprites_nearest/lab_light/29.png</key>
            <key type="filename">sprites_nearest/lab_light/3.png</key>
            <key type="filename">sprites_nearest/lab_light/30.png</key>
            <key type="filename">sprites_nearest/lab_light/31.png</key>
            <key type="filename">sprites_nearest/lab_light/32.png</key>
            <key type="filename">sprites_nearest/lab_light/33.png</key>
            <key type="filename">sprites_nearest/lab_light/34.png</key>
            <key type="filename">sprites_nearest/lab_light/35.png</key>
            <key type="filename">sprites_nearest/lab_light/36.png</key>
            <key type="filename">sprites_nearest/lab_light/37.png</key>
            <key type="filename">sprites_nearest/lab_light/38.png</key>
            <key type="filename">sprites_nearest/lab_light/39.png</key>
            <key type="filename">sprites_nearest/lab_light/4.png</key>
            <key type="filename">sprites_nearest/lab_light/40.png</key>
            <key type="filename">sprites_nearest/lab_light/41.png</key>
            <key type="filename">sprites_nearest/lab_light/42.png</key>
            <key type="filename">sprites_nearest/lab_light/43.png</key>
            <key type="filename">sprites_nearest/lab_light/44.png</key>
            <key type="filename">sprites_nearest/lab_light/45.png</key>
            <key type="filename">sprites_nearest/lab_light/46.png</key>
            <key type="filename">sprites_nearest/lab_light/47.png</key>
            <key type="filename">sprites_nearest/lab_light/5.png</key>
            <key type="filename">sprites_nearest/lab_light/6.png</key>
            <key type="filename">sprites_nearest/lab_light/7.png</key>
            <key type="filename">sprites_nearest/lab_light/8.png</key>
            <key type="filename">sprites_nearest/lab_light/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,4,8,8</rect>
                <key>scale9Paddings</key>
                <rect>4,4,8,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/lab_progress.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,1,31,1</rect>
                <key>scale9Paddings</key>
                <rect>16,1,31,1</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/lobby_board.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>15,11,31,23</rect>
                <key>scale9Paddings</key>
                <rect>15,11,31,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/lobby_key.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>1,2,1,5</rect>
                <key>scale9Paddings</key>
                <rect>1,2,1,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/lowerwall_1.png</key>
            <key type="filename">sprites_nearest/lowerwall_2.png</key>
            <key type="filename">sprites_nearest/lowerwall_3.png</key>
            <key type="filename">sprites_nearest/lowerwall_4.png</key>
            <key type="filename">sprites_nearest/lowerwall_5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,6,20,13</rect>
                <key>scale9Paddings</key>
                <rect>10,6,20,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/new_extension.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>26,26,51,51</rect>
                <key>scale9Paddings</key>
                <rect>26,26,51,51</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/roof_1.png</key>
            <key type="filename">sprites_nearest/roof_4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>16,14,31,28</rect>
                <key>scale9Paddings</key>
                <rect>16,14,31,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/roof_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>18,7,35,14</rect>
                <key>scale9Paddings</key>
                <rect>18,7,35,14</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/roof_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,4,27,8</rect>
                <key>scale9Paddings</key>
                <rect>14,4,27,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/roof_5.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,9,7,18</rect>
                <key>scale9Paddings</key>
                <rect>3,9,7,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/roof_6.png</key>
            <key type="filename">sprites_nearest/roof_7.png</key>
            <key type="filename">sprites_nearest/roof_8.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>4,9,7,18</rect>
                <key>scale9Paddings</key>
                <rect>4,9,7,18</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/serviceroom_alcool.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/0.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/1.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/10.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/11.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/12.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/13.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/14.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/15.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/16.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/17.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/18.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/19.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/2.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/20.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/21.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/22.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/23.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/24.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/25.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/26.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/27.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/28.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/29.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/3.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/30.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/31.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/32.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/33.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/34.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/35.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/36.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/37.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/38.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/39.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/4.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/40.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/41.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/42.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/43.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/44.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/45.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/46.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/47.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/48.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/49.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/5.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/50.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/51.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/52.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/53.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/54.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/55.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/56.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/57.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/58.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/59.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/6.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/60.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/61.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/62.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/63.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/64.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/65.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/66.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/67.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/68.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/69.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/7.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/70.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/71.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/72.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/73.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/74.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/75.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/76.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/8.png</key>
            <key type="filename">sprites_nearest/serviceroom_alcool/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>26,15,53,31</rect>
                <key>scale9Paddings</key>
                <rect>26,15,53,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/serviceroom_fridge.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/0.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/1.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/10.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/11.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/12.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/13.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/14.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/15.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/16.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/17.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/18.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/19.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/2.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/20.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/21.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/22.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/23.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/3.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/4.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/5.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/6.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/7.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/8.png</key>
            <key type="filename">sprites_nearest/serviceroom_fridge/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,17,17,35</rect>
                <key>scale9Paddings</key>
                <rect>9,17,17,35</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/serviceroom_shoe.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/0.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/1.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/10.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/11.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/12.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/13.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/14.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/15.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/16.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/17.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/18.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/19.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/2.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/20.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/21.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/22.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/23.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/24.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/25.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/3.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/4.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/5.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/6.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/7.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/8.png</key>
            <key type="filename">sprites_nearest/serviceroom_shoe/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>19,16,39,31</rect>
                <key>scale9Paddings</key>
                <rect>19,16,39,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/serviceroom_wash.png</key>
            <key type="filename">sprites_nearest/serviceroom_wash/0.png</key>
            <key type="filename">sprites_nearest/serviceroom_wash/1.png</key>
            <key type="filename">sprites_nearest/serviceroom_wash/10.png</key>
            <key type="filename">sprites_nearest/serviceroom_wash/11.png</key>
            <key type="filename">sprites_nearest/serviceroom_wash/2.png</key>
            <key type="filename">sprites_nearest/serviceroom_wash/3.png</key>
            <key type="filename">sprites_nearest/serviceroom_wash/4.png</key>
            <key type="filename">sprites_nearest/serviceroom_wash/5.png</key>
            <key type="filename">sprites_nearest/serviceroom_wash/6.png</key>
            <key type="filename">sprites_nearest/serviceroom_wash/7.png</key>
            <key type="filename">sprites_nearest/serviceroom_wash/8.png</key>
            <key type="filename">sprites_nearest/serviceroom_wash/9.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,15,25,30</rect>
                <key>scale9Paddings</key>
                <rect>12,15,25,30</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/specialroom.png</key>
            <key type="filename">sprites_nearest/specialroom_bg.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>21,21,42,42</rect>
                <key>scale9Paddings</key>
                <rect>21,21,42,42</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/specialroom_bin_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,8,11,15</rect>
                <key>scale9Paddings</key>
                <rect>6,8,11,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/specialroom_bin_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,5,27,9</rect>
                <key>scale9Paddings</key>
                <rect>14,5,27,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/specialroom_disco.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,10,20,21</rect>
                <key>scale9Paddings</key>
                <rect>10,10,20,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/specialroom_furnace.png</key>
            <key type="filename">sprites_nearest/specialroom_pool.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,15,55,31</rect>
                <key>scale9Paddings</key>
                <rect>27,15,55,31</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/specialroom_restaurant.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>5,11,9,23</rect>
                <key>scale9Paddings</key>
                <rect>5,11,9,23</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/tree_1.png</key>
            <key type="filename">sprites_nearest/tree_2.png</key>
            <key type="filename">sprites_nearest/tree_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>70,79,141,157</rect>
                <key>scale9Paddings</key>
                <rect>70,79,141,157</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/wall.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,8,5,15</rect>
                <key>scale9Paddings</key>
                <rect>3,8,5,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/wallpaper_1.png</key>
            <key type="filename">sprites_nearest/wallpaper_2.png</key>
            <key type="filename">sprites_nearest/wallpaper_3.png</key>
            <key type="filename">sprites_nearest/wallpaper_4.png</key>
            <key type="filename">sprites_nearest/wallpaper_5.png</key>
            <key type="filename">sprites_nearest/wallpaper_6.png</key>
            <key type="filename">sprites_nearest/wallpaper_7.png</key>
            <key type="filename">sprites_nearest/wallpaper_8.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,6,12,12</rect>
                <key>scale9Paddings</key>
                <rect>6,6,12,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/work_0.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,14,25,29</rect>
                <key>scale9Paddings</key>
                <rect>12,14,25,29</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/work_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>13,23,25,45</rect>
                <key>scale9Paddings</key>
                <rect>13,23,25,45</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/work_2.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,14,16,28</rect>
                <key>scale9Paddings</key>
                <rect>8,14,16,28</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/work_3.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,4,21,8</rect>
                <key>scale9Paddings</key>
                <rect>10,4,21,8</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sprites_nearest/work_4.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>10,8,21,15</rect>
                <key>scale9Paddings</key>
                <rect>10,8,21,15</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>sprites_nearest</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
