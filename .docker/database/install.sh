#!/bin/bash

set -e
set -u

function create_user_and_database() {
	local database=$1
	local password=$2
	echo "  Creating database '$database' and user '$database.admin'"
	psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
	    CREATE DATABASE "$database";
	    CREATE USER "$database.admin" PASSWORD '$password';
	    GRANT ALL PRIVILEGES ON DATABASE "$database" TO "$database.admin";
EOSQL
  psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$database" <<-EOSQL
      ALTER SCHEMA "public" OWNER TO "$database.admin";
EOSQL
}

if [ -n "$POSTGRES_INIT_DATABASES" ]; then
	echo "Databases creation requested: $POSTGRES_INIT_DATABASES"
	for db in $(echo $POSTGRES_INIT_DATABASES | tr ',' ' '); do
		create_user_and_database $db $POSTGRES_PASSWORD
	done
	echo "Databases created"
fi
