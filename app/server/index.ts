import "reflect-metadata";

import { listen, setup } from "./main";

setup().then(listen);
