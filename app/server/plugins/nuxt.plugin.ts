import { NuxtConfig } from "@nuxt/types/config";
import { FastifyPluginAsync, FastifyReply, FastifyRequest, RouteShorthandMethod, RouteShorthandOptions } from "fastify";
import fp from "fastify-plugin";
import Nuxt, { NuxtInstance, NuxtRenderRouteContext } from "nuxt";

const { loadNuxt, build } = Nuxt;

export interface FastifyNuxtPluginOptions {
  config: NuxtConfig
}

export interface FastifyNuxtPluginRenderOptions extends RouteShorthandOptions {
  method?: string,
}

export type FastifyNuxtCallback = (
  app: NuxtInstance,
  req: FastifyRequest,
  reply: FastifyReply
) => Promise<void>;

declare module "fastify" {
  interface FastifyInstance {
    nuxt: (path: string, opts?: FastifyNuxtPluginRenderOptions | FastifyNuxtCallback, callback?: FastifyNuxtCallback) => void;
  }

  interface FastifyReply {
    nuxt: () => Promise<FastifyReply>
    sendNuxtRenderedRoute: (route?: string | NuxtRenderRouteContext, context?: NuxtRenderRouteContext) => Promise<FastifyReply>
  }
}

const fastifyNuxt: FastifyPluginAsync<FastifyNuxtPluginOptions> = fp(async (fastify, _options) => {
  const isDev = process.env.NODE_ENV !== "production";
  // const publicPath = options.config.publicPath ? `${options.config.publicPath}*` : "/_nuxt/*";

  const nuxtPromise = loadNuxt({ for: isDev ? "dev" : "start" });
  const nuxt = await nuxtPromise;

  if (isDev) {
    build(nuxt);
  }

  fastify
    .decorate("nuxt", handle)
    .decorateReply("nuxt", render)
    .decorateReply("sendNuxtRenderedRoute", sendNuxtRenderedRoute)
    .addHook("onClose", function () {
      nuxt.close();
    });

  handle("/_nuxt/*");

  if (isDev) {
    handle("/__webpack_hmr/*");
  }

  function handle(path: string, opts?: FastifyNuxtPluginRenderOptions | FastifyNuxtCallback, callback?: FastifyNuxtCallback) {
    opts = opts || { };

    if (typeof (opts) === "function") {
      callback = opts;
      opts = { };
    }

    const method = opts.method ?? "get";

    ((fastify as any)[method.toLowerCase()] as RouteShorthandMethod)(path, opts, handler);

    async function handler(request: FastifyRequest, reply: FastifyReply) {
      for (const [headerName, headerValue] of Object.entries(reply.getHeaders())) {
        reply.raw.setHeader(headerName, headerValue!);
      }

      const nuxt = await nuxtPromise;

      if (callback) {
        callback(nuxt, request, reply);
        return;
      }

      nuxt.render(request.raw, reply.raw);
      reply.sent = true;
    }
  }

  async function render(this: FastifyReply) {
    for (const [headerName, headerValue] of Object.entries(this.getHeaders())) {
      this.raw.setHeader(headerName, headerValue!);
    }

    const nuxt = await nuxtPromise;

    nuxt.render(this.request.raw, this.raw);
    this.sent = true;

    return this;
  }

  async function sendNuxtRenderedRoute(this: FastifyReply, route?: string | NuxtRenderRouteContext, context?: NuxtRenderRouteContext): Promise<FastifyReply> {
    if (typeof (route) === "object") {
      context = route;
      route = undefined;
    }

    if (!route) {
      route = this.request.routerPath;
    }

    try {
      const result = await nuxt.renderRoute(route, context);
      this.type("text/html")
        .status(result.error?.statusCode ?? 200)
        .send(result.html);
    } catch (e) {
      this.status(500).send(e);
    }
    return this;
  }
}, "3.x");

export default fastifyNuxt;
