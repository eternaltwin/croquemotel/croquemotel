import "reflect-metadata";

import { TestInterface } from "ava";
import { FastifyInstance } from "fastify";
import LightMyRequest from "light-my-request";

import { setup } from "./main";

export function setupServer(anyTest: TestInterface) {
  const test = anyTest as TestInterface<{
    server: FastifyInstance,
    createSessionCookies: (auth: boolean | Record<string, any>, data?: Record<string, any>) => { [k: string]: string },
    cookiesFrom: (response: LightMyRequest.Response) => { [k: string]: string; } | undefined
  }>;

  test.before("Starting Fastify server", async (t) => {
    const server = await setup({ client: false });

    t.context.server = server;

    t.context.createSessionCookies = function (auth, data) {
      if (typeof auth === "object") {
        data = auth;
        auth = false;
      }

      data = data || {};

      const cookies: { [k: string]: string } = {};

      if (auth) {
        data.authToken = "randomauthtokenfortest";
        cookies.auth = "1";
      }

      const session = server.createSecureSession(data);
      cookies.session = server.encodeSecureSession(session);

      return cookies;
    };

    t.context.cookiesFrom = function (response) {
      const cookies = response.cookies as Array<{ name: string, value: string }>;
      return cookies?.reduce(function (result, cookie) {
        result[cookie.name] = cookie.value;
        return result;
      }, {} as { [k: string]: string; });
    };
  });

  test.after.always("Stopping Fastify server", async (t) => {
    await t.context.server.close();
  });

  return test;
}
