import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";

import { IGuest } from "../../../contracts/guest.interface";
import { GuestLikes } from "../../../contracts/guest-likes.enum";
import { GuestType } from "../../../contracts/guest-type.enum";
import { Hotel } from "../../hotel/entities/hotel.entity";
import { GuestTypeTransformer } from "../transformers/guest-type.transformer";

@Entity()
export class Guest implements IGuest {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  hotelId!: string;

  @ManyToOne("Hotel")
  @JoinColumn({ name: "hotelId" })
  hotel!: Hotel;

  @Column({ type: "varchar", transformer: new GuestTypeTransformer() })
  type!: GuestType;

  @Column()
  name!: string;

  @Column({ enum: GuestLikes })
  likes!: GuestLikes;

  @Column({ enum: GuestLikes })
  dislikes!: GuestLikes;

  @Column({ enum: GuestLikes })
  effects!: GuestLikes;

  @Column({ default: false })
  isVip!: boolean;

  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;
}
