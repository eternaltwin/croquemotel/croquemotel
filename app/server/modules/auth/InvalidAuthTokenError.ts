export class InvalidAuthTokenError extends Error {
  constructor(message?: string) {
    super(message ?? "Invalid auth token");
  }

  public get statusCode() {
    return 401;
  }
}
