import { getInstanceByToken } from "fastify-decorators";

import { AuthService } from "./auth.service";

export const authenticate = getInstanceByToken<AuthService>(AuthService).authenticate;
