import { FastifyReply, FastifyRequest } from "fastify";
import { Controller, POST } from "fastify-decorators";

import { AuthService } from "../auth.service";

@Controller("/auth/login")
export default class LoginHandler {
  constructor(
    private authService: AuthService
  ) { }

  @POST()
  public async post(_request: FastifyRequest, reply: FastifyReply) {
    const csrfToken = await reply.generateCsrf();
    const authorizationUri = this.authService.generateAuthorizationUri(`${process.env.APP_URI}/auth/callback`, csrfToken);
    reply.redirect(authorizationUri);
  }
}
