import { FastifyReply, FastifyRequest } from "fastify";
import { Controller, POST } from "fastify-decorators";

@Controller("/auth/logout")
export default class LogoutHandler {
  @POST()
  public post(_request: FastifyRequest, reply: FastifyReply) {
    reply.deleteSession();
    reply.code(204).send();
  }
}
