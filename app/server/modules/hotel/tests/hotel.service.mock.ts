import { DatabaseServiceMock } from "../../../tests/database.service.mock";
import { HotelService } from "../hotel.service";

export class HotelServiceMock {
  private service?: HotelService;

  constructor(private db: DatabaseServiceMock) { }

  async create() {
    this.service = new HotelService(this.db as any);
    await this.service.init();
  }

  get mock() {
    return {
      provide: HotelService,
      useValue: this.service as any
    };
  }
}
