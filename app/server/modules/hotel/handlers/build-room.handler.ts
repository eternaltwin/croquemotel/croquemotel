import { Static, Type } from "@sinclair/typebox";
import { FastifyReply, FastifyRequest } from "fastify";
import { Controller, PATCH, POST } from "fastify-decorators";

import { ServiceResult } from "../../../services/service";
import { authenticate } from "../../auth/auth.helper";
import { BuildService } from "../build.service";
import { Room } from "../entities/room.entity";

const PostBuildRoomBody = Type.Union([
  Type.Object({
    roomId: Type.Number({ minimum: 1 }),
    type: Type.Literal("bed")
  }),
  Type.Object({
    roomId: Type.Number({ minimum: 1 }),
    type: Type.Union([
      Type.Literal("service"),
      Type.Literal("special")
    ]),
    service: Type.String()
  })
]);
type PostBuildRoomBody = Static<typeof PostBuildRoomBody>;

const PatchBuildRoomBody = Type.Object({
  room: Type.Number({ minimum: 1 }),
  type: Type.Literal("bed")
});
type PatchBuildRoomBody = Static<typeof PatchBuildRoomBody>;

@Controller("/api/hotel/build/room")
export default class BuildRoomHandler {
  constructor(
    private buildService: BuildService
  ) { }

  @POST("", {
    preHandler: authenticate,
    schema: {
      body: PostBuildRoomBody
    }
  })
  public async post(request: FastifyRequest<{ Body: PostBuildRoomBody }>, reply: FastifyReply) {
    const hotelId = request.getHotelId()!;

    let result: ServiceResult<Room>;

    switch (request.body.type) {
      case "bed":
        result = await this.buildService.buildNewBedRoom(hotelId, request.body.roomId);
        break;
      case "service":
        result = await this.buildService.buildNewServiceRoom(hotelId, request.body.roomId, request.body.service!);
        break;
      case "special":
        result = await this.buildService.buildNewSpecialRoom(hotelId, request.body.roomId, request.body.service!);
        break;
      default:
        return;
    }

    reply.sendServiceResult(result, { groups: ["player"], successCode: 201 });
  }

  @PATCH("", {
    preHandler: authenticate,
    schema: {
      body: PatchBuildRoomBody
    }
  })
  public async patch(request: FastifyRequest<{ Body: PatchBuildRoomBody }>, reply: FastifyReply) {
    const hotelId = request.getHotelId()!;

    let result: ServiceResult<Room>;

    switch (request.body.type) {
      case "bed":
        result = await this.buildService.upgradeBedRoom(hotelId, request.body.room);
        break;
      default:
        return;
    }

    reply.sendServiceResult(result, { groups: ["player"] });
  }
}
