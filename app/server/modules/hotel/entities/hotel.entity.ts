import { Expose } from "class-transformer";
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn
} from "typeorm";

import { IHotelPlayer } from "../../../contracts/hotel.interface";
import { NeonColor } from "../../../contracts/neon-color.enum";
import { Guest } from "../../guest/entities/guest.entity";
import { Floor } from "./floor.entity";
import { Staff } from "./staff.entity";

@Entity()
export class Hotel implements IHotelPlayer {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  playerId!: string;

  @OneToMany("Floor", "hotel", { cascade: true })
  floors?: Floor[];

  @Column()
  name!: string;

  @Column({ default: 0 })
  stars!: number;

  @Column({ default: 0 })
  @Expose({ groups: ["player"] })
  money!: number;

  @Column({ default: 0 })
  fame!: number;

  @Column({ default: 0 })
  @Expose({ groups: ["player"] })
  level!: number;

  @Column({ enum: NeonColor, default: NeonColor.red })
  neonColor!: NeonColor;

  @OneToMany("Staff", "hotel", { cascade: true })
  @Expose({ groups: ["player"] })
  staffs?: Staff[];

  @OneToMany("Guest", "hotel", { cascade: true })
  waitingList?: Guest[];

  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;
}
