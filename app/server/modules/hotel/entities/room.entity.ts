import {
  ChildEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  TableInheritance,
  UpdateDateColumn
} from "typeorm";

import { BedRoomLevel } from "../../../contracts/bedroom-level.enum";
import { LobbyRoomLevel } from "../../../contracts/lobbyroom-level.enum";
import { IBedRoom, IEmptyRoom, ILobbyRoom, IRoom, IServiceRoom, ISpecialRoom, RoomType } from "../../../contracts/room.interface";
import { RoomEquipment } from "../../../contracts/room-equipment.enum";
import { ServiceRoomType } from "../../../contracts/serviceroom-type.enum";
import { SpecialRoomType } from "../../../contracts/specialroom-type.enum";
import { Guest } from "../../guest/entities/guest.entity";
import { BedRoomLevelTransformer } from "../transformers/bedroom-level.transformer";
import { LobbyRoomLevelTransformer } from "../transformers/lobbyroom-level.transformer";
import { RoomEquipmentTransformer } from "../transformers/room-equipment.transformer";
import { ServiceRoomTypeTransformer } from "../transformers/serviceroom-type.transformer";
import { SpecialRoomTypeTransformer } from "../transformers/specialroom-type.transformer";
import { Floor } from "./floor.entity";
import { Staff } from "./staff.entity";

@Entity()
@TableInheritance({ column: { type: "varchar", name: "type" } })
export abstract class Room implements IRoom {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  type!: RoomType;

  @Column()
  floorId!: number;

  @ManyToOne("Floor", "rooms")
  @JoinColumn({ name: "floorId" })
  floor?: Floor;

  @Column({ default: 0 })
  position!: number;

  @OneToMany("Staff", "room")
  staffs?: Staff[];

  @Column({ nullable: true })
  buildingEndAt?: Date;

  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;

  get isBuilt(): boolean {
    return typeof this.buildingEndAt === "undefined" || this.buildingEndAt < new Date();
  }
}

@ChildEntity("empty")
export class EmptyRoom extends Room implements IEmptyRoom {

}

@ChildEntity("bed")
export class BedRoom extends Room implements IBedRoom {
  @Column({ type: "int", default: 1, transformer: new BedRoomLevelTransformer() })
  level!: BedRoomLevel;

  @Column({ default: 0 })
  damages!: number;

  @OneToOne("Guest", { nullable: true })
  @JoinColumn()
  guest?: Guest;

  @Column({ type: "varchar", nullable: true, transformer: new RoomEquipmentTransformer() })
  equipment1?: RoomEquipment;

  @Column({ type: "varchar", nullable: true, transformer: new RoomEquipmentTransformer() })
  equipment2?: RoomEquipment;

  @Column({ nullable: true })
  serviceEndAt?: Date;
}

@ChildEntity("special")
export class SpecialRoom extends Room implements ISpecialRoom {
  @Column({ type: "varchar", transformer: new SpecialRoomTypeTransformer() })
  service!: SpecialRoomType;

  @Column({ nullable: true })
  serviceEndAt?: Date;

  @OneToOne("Guest", { nullable: true })
  @JoinColumn()
  guest?: Guest;

  @Column()
  progress?: number;
}

@ChildEntity("service")
export class ServiceRoom extends Room implements IServiceRoom {
  @Column({ type: "varchar", transformer: new ServiceRoomTypeTransformer() })
  service!: ServiceRoomType;

  @Column({ nullable: true })
  serviceEndAt?: Date;

  @OneToOne("Guest", { nullable: true })
  @JoinColumn()
  guest?: Guest;
}

@ChildEntity("lobby")
export class LobbyRoom extends Room implements ILobbyRoom {
  @Column({ type: "int", default: 1, transformer: new LobbyRoomLevelTransformer() })
  level!: LobbyRoomLevel;
}
