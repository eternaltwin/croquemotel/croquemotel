import { ValueTransformer } from "typeorm";

import { ServiceRoomType } from "../../../contracts/serviceroom-type.enum";

export class ServiceRoomTypeTransformer implements ValueTransformer {
  to(value: ServiceRoomType): any {
    return value?.name;
  }

  from(value: any): ServiceRoomType {
    return ServiceRoomType.from(value);
  }
}
