import { ValueTransformer } from "typeorm";

import { SpecialRoomType } from "../../../contracts/specialroom-type.enum";

export class SpecialRoomTypeTransformer implements ValueTransformer {
  to(value: SpecialRoomType): any {
    return value?.name;
  }

  from(value: any): SpecialRoomType {
    return SpecialRoomType.from(value);
  }
}
