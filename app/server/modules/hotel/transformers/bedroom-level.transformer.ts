import { ValueTransformer } from "typeorm";

import { BedRoomLevel } from "../../../contracts/bedroom-level.enum";

export class BedRoomLevelTransformer implements ValueTransformer {
  to(value: BedRoomLevel): any {
    return value?.level;
  }

  from(value: any): BedRoomLevel {
    return BedRoomLevel.from(value);
  }
}
