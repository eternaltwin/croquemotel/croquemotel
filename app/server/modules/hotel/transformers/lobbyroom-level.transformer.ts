import { ValueTransformer } from "typeorm";

import { LobbyRoomLevel } from "../../../contracts/lobbyroom-level.enum";

export class LobbyRoomLevelTransformer implements ValueTransformer {
  to(value: LobbyRoomLevel): any {
    return value?.level;
  }

  from(value: any): LobbyRoomLevel {
    return LobbyRoomLevel.from(value);
  }
}
