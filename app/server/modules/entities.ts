// PATCH: https://stackoverflow.com/questions/66735359/error-when-using-typeorm-and-typescript-with-folder-path
export * from "./guest/entities/guest.entity";
export * from "./hotel/entities/floor.entity";
export * from "./hotel/entities/hotel.entity";
export * from "./hotel/entities/room.entity";
export * from "./hotel/entities/staff.entity";
