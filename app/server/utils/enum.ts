export abstract class Enum {
  public static addFlag(value: number, flag: number): number {
    return value | flag;
  }

  public static removeFlag(value: number, flag: number): number {
    return value & ~flag;
  }

  public static hasFlag(value: number, flag: number): boolean {
    return (value & flag) === flag;
  }

  public static hasOnlyFlag(value: number, flag: number): boolean {
    return Enum.hasOnlyOneFlag(value) && Enum.hasFlag(value, flag);
  }

  public static hasOnlyOneFlag(value: number): boolean {
    return value !== 0 && (value & (value - 1)) === 0;
  }
}
