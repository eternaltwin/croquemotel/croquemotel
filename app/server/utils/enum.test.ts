import test from "ava";

import { Enum } from "./enum";

enum TestEnum {
  none = 0,
  alpha = 1 << 0, // 1
  beta = 1 << 1, // 2
  gamma = 1 << 2, // 4
}

test("Enum.addFlag", (t) => {
  const value = TestEnum.alpha;
  const newValue = Enum.addFlag(value, TestEnum.beta);

  t.assert(newValue === 3);
  t.assert(newValue === (TestEnum.alpha | TestEnum.beta));
});

test("Enum.removeFlag", (t) => {
  const value = TestEnum.alpha | TestEnum.beta | TestEnum.gamma;
  const newValue = Enum.removeFlag(value, TestEnum.beta);

  t.assert(newValue === 5);
  t.assert(newValue === (TestEnum.alpha | TestEnum.gamma));
});

test("Enum.hasFlag", (t) => {
  const value = TestEnum.alpha | TestEnum.gamma;

  t.assert(Enum.hasFlag(value, TestEnum.alpha) === true);
  t.assert(Enum.hasFlag(value, TestEnum.beta) === false);
  t.assert(Enum.hasFlag(value, TestEnum.gamma) === true);
});

test("Enum.hasOnlyFlag", (t) => {
  const value1 = TestEnum.alpha | TestEnum.gamma;
  const value2 = TestEnum.alpha;

  t.assert(Enum.hasOnlyFlag(value1, TestEnum.alpha) === false);
  t.assert(Enum.hasOnlyFlag(value1, TestEnum.gamma) === false);

  t.assert(Enum.hasOnlyFlag(value2, TestEnum.alpha) === true);
  t.assert(Enum.hasOnlyFlag(value2, TestEnum.gamma) === false);
});

test("Enum.hasOnlyOneFlag", (t) => {
  const value0 = TestEnum.none;
  const value1 = TestEnum.alpha;
  const value2 = TestEnum.alpha | TestEnum.gamma;

  t.assert(Enum.hasOnlyOneFlag(value0) === false);
  t.assert(Enum.hasOnlyOneFlag(value1) === true);
  t.assert(Enum.hasOnlyOneFlag(value2) === false);
});
