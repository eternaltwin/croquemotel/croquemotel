export abstract class BaseService {
  /**
   * Creates a successful service result with a resource.
   * @param resource The ressource from the service call.
   * @returns A successful service result.
   */
  protected succeed<T>(resource: T): ServiceResult<T> {
    return _ServiceResult.succeed(resource);
  }

  /**
   * Creates an erroneous service result with error messages.
   * @param status The error status (`ServiceResultStatus.error` by default).
   * @param messages Error message(s) to bind.
   * @returns An erroneous service result.
   */
  protected fail<T>(status?: ServiceResultStatus | string[] | string, messages?: string[] | string): ServiceResult<T> {
    return _ServiceResult.fail(status, messages);
  }

  /**
   * Creates a resource not found erroneous service result with error messages.
   * @param messages Error message(s) to bind.
   * @returns An erroneous service result.
   */
  protected failNotFound<T>(messages?: string[] | string): ServiceResult<T> {
    return _ServiceResult.fail(ServiceResultStatus.notFound, messages);
  }

  /**
   * Creates an unauthorized (user not logged in) erroneous service result with error messages.
   * @param messages Error message(s) to bind.
   * @returns An erroneous service result.
   */
  protected failUnauthorized<T>(messages?: string[] | string): ServiceResult<T> {
    return _ServiceResult.fail(ServiceResultStatus.unauthorized, messages);
  }

  /**
   * Creates a forbidden erroneous service result with error messages.
   * @param messages Error message(s) to bind.
   * @returns An erroneous service result.
   */
  protected failForbidden<T>(messages?: string[] | string): ServiceResult<T> {
    return _ServiceResult.fail(ServiceResultStatus.forbidden, messages);
  }

  /**
   * Creates an invalid erroneous service result with error messages.
   * @param messages Error message(s) to bind.
   * @returns An erroneous service result.
   */
  protected failInvalid<T>(messages?: string[] | string): ServiceResult<T> {
    return _ServiceResult.fail(ServiceResultStatus.invalid, messages);
  }
}

export enum ServiceResultStatus {
  success = 200,
  error = 500,
  notFound = 404,
  unauthorized = 401,
  forbidden = 403,
  invalid = 400,
}

export type ServiceResult<T> = _ServiceResult<T | undefined>;

class _ServiceResult<T> {
  private constructor(
    public status: ServiceResultStatus,
    public messages: string[],
    public value: T
  ) { }

  /**
   * Tells if the result is a success.
   */
  public get succeeded() {
    return this.status === ServiceResultStatus.success;
  }

  /**
   * Creates a successful service result with a value.
   * @param value The ressource from the service call.
   * @returns A successful service result.
   */
  public static succeed<T>(value: T): ServiceResult<T> {
    return new _ServiceResult(
      ServiceResultStatus.success,
      [],
      value
    );
  }

  /**
   * Creates an erroneous service result with error messages.
   * @param status The error status (`ServiceResultStatus.error` by default).
   * @param messages Error message(s) to bind.
   * @returns An erroneous service result.
   */
  public static fail(status?: ServiceResultStatus | string[] | string, messages?: string[] | string): ServiceResult<undefined> {
    if (typeof status !== "number") {
      messages = status;
      status = ServiceResultStatus.error;
    }

    if (!messages) {
      messages = [];
    } else if (!Array.isArray(messages)) {
      messages = [messages];
    }

    return new _ServiceResult(
      status,
      messages,
      undefined
    );
  }

  /**
   * Cast the current erroneous service status, to simplify returns from service to service.
   * @returns A new erroneous service result with original status and error messages.
   */
  public castFailed<T>(): ServiceResult<T> {
    return new _ServiceResult(
      this.status,
      this.messages,
      undefined
    );
  }
}
