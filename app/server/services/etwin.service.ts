import { AccessTokenAuthContext } from "@eternal-twin/core/lib/auth/access-token-auth-context";
import { ShortUser } from "@eternal-twin/core/lib/user/short-user";
import { HttpEtwinClient } from "@eternal-twin/etwin-client-http";
import { Service } from "fastify-decorators";
import { URL } from "url";

import { BaseService } from "./service";

@Service()
export class EtwinService extends BaseService {
  private _client!: HttpEtwinClient;

  constructor() {
    super();
    this._client = new HttpEtwinClient(new URL(process.env.ETWIN_API_URI!));
  }

  public getAuthSelf(accessToken: string): Promise<AccessTokenAuthContext> {
    return this._client.getAuthSelf(accessToken) as Promise<AccessTokenAuthContext>;
  }

  public getUserById(accessToken: string | null, userId: string): Promise<ShortUser> {
    return this._client.getUserById(accessToken, userId);
  }
}
