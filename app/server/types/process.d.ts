/**
 * Extends NodeJS.Process interface
 */

declare namespace NodeJS {
  interface Process {
    dev: boolean;
    test: boolean;
    bindAddress: string;
    bindPort: string | number;
    buildNuxt: () => Promise<void>;
  }
}
