export enum GuestLikes {
  none = 0,
  neighbors = 1 << 0,
  humidity = 1 << 1,
  heat = 1 << 2,
  noise = 1 << 3,
  smells = 1 << 4,
  food = 1 << 5,
  zombie = 1 << 6,
  deluxeRoom = 1 << 7,
}
