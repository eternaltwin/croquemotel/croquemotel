import { RoomBuildConfig } from "./room-build-config.interface";

export class LobbyRoomLevel {
  static ZERO = new LobbyRoomLevel(0, {
    price: 0,
    delay: 0,
    fame: 0,
    requiredHotelLevel: 0,
    disallowFloor0: false
  });

  static ONE = new LobbyRoomLevel(1, {
    price: 250,
    delay: 0,
    fame: 0,
    requiredHotelLevel: 0,
    disallowFloor0: false
  });

  static TWO = new LobbyRoomLevel(2, {
    price: 500,
    delay: 0,
    fame: 0,
    requiredHotelLevel: 0,
    disallowFloor0: false
  });

  static THREE = new LobbyRoomLevel(3, {
    price: 1500,
    delay: 0,
    fame: 0,
    requiredHotelLevel: 0,
    disallowFloor0: false
  });

  static FOUR = new LobbyRoomLevel(3, {
    price: 3000,
    delay: 0,
    fame: 0,
    requiredHotelLevel: 0,
    disallowFloor0: false
  });

  private constructor(
    public level: number,
    public buildConfig: RoomBuildConfig
  ) { }

  static from(level: number): LobbyRoomLevel {
    return Object.values(LobbyRoomLevel).find(value => value instanceof LobbyRoomLevel && value.level === level);
  }
}
