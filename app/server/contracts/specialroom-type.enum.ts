import { RoomBuildConfig } from "./room-build-config.interface";

export class SpecialRoomType {
  static RESTAURANT = new SpecialRoomType("restaurant", {
    delay: 30 * 60 * 1000,
    price: count => 200 + count * 100,
    requiredHotelLevel: 3
  });

  static BIN = new SpecialRoomType("bin", {
    delay: 30 * 60 * 1000,
    price: count => 200 + count * 100,
    requiredHotelLevel: 3
  });

  static DISCO = new SpecialRoomType("disco", {
    delay: 30 * 60 * 1000,
    price: count => 200 + count * 100,
    requiredHotelLevel: 3
  });

  static FURNACE = new SpecialRoomType("furnace", {
    delay: 30 * 60 * 1000,
    price: count => 200 + count * 100,
    requiredHotelLevel: 3
  });

  static POOL = new SpecialRoomType("pool", {
    delay: 30 * 60 * 1000,
    price: count => 200 + count * 100,
    requiredHotelLevel: 3
  });

  static LAB = new SpecialRoomType("lab", {
    delay: 30 * 60 * 1000,
    price: 250,
    max: 2,
    requiredHotelLevel: 2,
    fame: 1
  });

  private constructor(
    public name: string,
    public buildConfig: RoomBuildConfig
  ) { }

  static from(name: string): SpecialRoomType {
    return Object.values(SpecialRoomType).find(value => value instanceof SpecialRoomType && value.name === name);
  }
}
