import { RoomBuildConfig } from "./room-build-config.interface";

export class ServiceRoomType {
  static ALCOOL = new ServiceRoomType("alcool", {
    price: 300,
    delay: 30 * 60 * 1000,
    requiredHotelLevel: 6
  });

  static FRIDGE = new ServiceRoomType("fridge", {
    price: 300,
    delay: 30 * 60 * 1000,
    requiredHotelLevel: 5
  });

  static SHOE = new ServiceRoomType("shoe", {
    price: 300,
    delay: 30 * 60 * 1000,
    requiredHotelLevel: 4
  });

  static WASH = new ServiceRoomType("wash", {
    price: 300,
    delay: 30 * 60 * 1000,
    requiredHotelLevel: 2
  });

  private constructor(
    public name: string,
    public buildConfig: RoomBuildConfig
  ) { }

  static from(name: string): ServiceRoomType {
    return Object.values(ServiceRoomType).find(value => value instanceof ServiceRoomType && value.name === name);
  }
}
