import { IFloor } from "./floor.interface";
import { IGuest } from "./guest.interface";
import { NeonColor } from "./neon-color.enum";
import { IStaff } from "./staff.interface";

export interface IHotel {
  id: number;
  playerId: string;
  floors?: IFloor[];
  name: string;
  stars: number;
  fame: number;
  neonColor: NeonColor;
  waitingList?: IGuest[];
  createdAt: Date;
  updatedAt: Date;
}

export interface IHotelPlayer extends IHotel {
  money: number;
  level: number;
  staffs?: IStaff[];
}
