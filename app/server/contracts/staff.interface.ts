import { IRoom } from "./room.interface";

export interface IStaff {
  id: number;
  hotelId: number;
  room?: IRoom;
  createdAt: Date;
  updatedAt: Date;
}
