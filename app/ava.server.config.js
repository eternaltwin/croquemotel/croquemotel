export default {
  files: [
    "./server/**/*.test.{ts,js}"
  ],
  extensions: {
    ts: "module"
  },
  nonSemVerExperiments: {
    configurableModuleFormat: true
  },
  nodeArguments: [
    "--experimental-specifier-resolution=node",
    "--loader=ts-node/esm"
  ],
  timeout: "1m",
  environmentVariables: {
    TS_NODE_PROJECT: "server/tsconfig.json",
    NODE_ENV: "test",
    APP_PORT: "8080",
    APP_URI: "http://localhost:8080",
    ETWIN_PORT: "50320",
    ETWIN_API_URI: "http://etwin:50320",
    ETWIN_OAUTH_URI: "http://etwin:50320",
    ETWIN_OAUTH_KEY: "croquemotel@clients",
    ETWIN_OAUTH_SECRET: "dev_secret",
    DB_ENGINE: "sqlite"
  }
};
