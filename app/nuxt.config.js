import { resolve } from "path";

export default {
  telemetry: false,
  rootDir: resolve(__dirname, "client"),
  srcDir: resolve(__dirname, "client"),
  buildDir: resolve(__dirname, "dist", ".nuxt"),

  buildModules: [
    "nuxt-build-optimisations",
    "@nuxt/typescript-build"
  ],

  build: {
    hotMiddleware: {
      client: {
        autoConnect: false
      }
    }
  },

  components: true,

  css: [
    "~/assets/styles/main.scss"
  ],

  plugins: [
    { src: "~/plugins/axios.ts" },
    { src: "~/plugins/axios-accessor.ts" },
    { src: "~/plugins/cookies-accessor.ts" },
    { src: "~/plugins/pixi.ts", mode: "client" }
  ],

  modules: [
    "@nuxtjs/axios",
    "nuxt-i18n",
    ["cookie-universal-nuxt", { parseJSON: false }]
  ],

  axios: {
    baseURL: "http://localhost:3000"
  },

  publicRuntimeConfig: {
    axios: {
      browserBaseURL: process.env.APP_URI
    }
  },

  router: {
    middleware: "auth"
  },

  i18n: {
    locales: [
      {
        code: "de",
        file: "de.js"
      },
      {
        code: "en",
        file: "en.js"
      },
      {
        code: "es",
        file: "es.js"
      },
      {
        code: "fr",
        file: "fr.js"
      }
    ],
    langDir: "lang/",
    lazy: true,
    defaultLocale: "en",
    strategy: "no_prefix",
    vueI18n: {
      fallbackLocale: "en"
    },
    vuex: false,
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: "lang"
    }
  },

  watchers: {
    webpack: {
      poll: true
    }
  }
};
