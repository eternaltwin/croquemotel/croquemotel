import { Middleware } from "@nuxt/types";

import { authStore } from "~/store";

const authMiddleware: Middleware = async ({ route, redirect }) => {
  await authStore.update();

  if (!route.meta?.[0]?.auth) {
    return;
  }

  if (!authStore.isLoggedIn) {
    return redirect("/login");
  }
};

export default authMiddleware;
