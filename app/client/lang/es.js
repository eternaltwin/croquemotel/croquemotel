export default {
  welcome: "Bienvenido",
  menu: {
    index: "Descubre el juego",
    play: "¡Juega ahora!",
    visit: "Visita un hotel al azar",
    help: "Guía",
    office: null,
    lab: null,
    shop: null,
    friends: null
  },
  hotel: {
    room: {
      none: {
        title: "Espacio libre para una habitación o sala",
        desc: "Poco a poco tu hotel crece...",
        info: "Utiliza este espacio vacío para instalar *nuevas habitaciones o servicios*."
      },
      bed: {
        title: "Habitación",
        desc: "Los clientes son realmente quejosos: ¡no quieren dormir en el pasillo ni en las escaleras!...",
        info: "Una habitación *alojará a un nuevo cliente*."
      },
      lobby: {
        title: "Recepción",
        desc: "Un viejo mueble de bar y llaves colgadas en la pared (todas iguales, por cierto) impresiona a los turistas.",
        info: "Más camareros de la Recepción ayudan a atraer clientes."
      },
      lobbySlot: {
        title: "Espacio libre para colocar una nueva Recepción",
        desc: "¡No hagas esperar a tus clientes!",
        info: "Aquí puedes construir una nueva *Recepción*."
      },
      restaurant: {
        title: "Restaurante astronómico",
        desc: "Sí, es astronómico, porque sus precios son demasiado elevados...",
        info: "Los clientes *glotones* gastarán una fortuna en él."
      },
      bin: {
        title: "Depósito de desperdicios",
        desc: "Es donde se \"guarda\" toda la basura.",
        info: "Los clientes que exigen *malos olores* pagarán por acceder a él."
      },
      disco: {
        title: "Discoteca",
        desc: "Un DJ hace mezclas de Reggaetón, Hip-Hop y los hits del verano a todo volúmen.",
        info: "Los clientes que disfrutan del *ruido* estarán ahí para gastar su dinero."
      },
      furnace: {
        title: "Crematorio",
        desc: "Es como un horno común, pero son los clientes quienes van adentro.",
        info: "Los clientes que aman *el calor* estarán felices de dormir ahí."
      },
      pool: {
        title: "Sauna",
        desc: "Una tecnología alienígena hace que el agua penetre hasta los huesos.",
        info: "Los clientes que disfrutan de *la humedad* estarán felices de usar este servicio."
      },
      wash: {
        title: "Lavamatix",
        desc: "Un modelo revolucionario que lava, seca, plancha y dobla la ropa ordenada por colores.",
        info: "Sirve para atender a los huéspedes que dejan su ropa sucia en la puerta."
      },
      shoe: {
        title: "Lustra-zapatos",
        desc: "Una máquina que deja los zapatos de tus huéspedes como un espejo.",
        info: "Limpia y lustra los zapatos que los clientes dejan frente a sus puertas."
      },
      alcool: {
        title: "Tragomático",
        desc: "Un distribuidor de licores conectado al desagüe de otro hotel... claro, tus camareros aseguran que es Whisky.",
        info: "Brinda bebidas a los clientes que lo soliciten."
      },
      fridge: {
        title: "Minibar Plus",
        desc: "Especialmente programado para ofrecer todo tipo de alimentos.",
        info: "Es el sueño de todo huésped glotón."
      },
      lab: {
        title: "Sala de Relax",
        desc: "Para conocer mejor a tus clientes, nada mejor que abrirlos para ver qué tienen dentro.",
        info: "Gana *Puntos laboratorio* sacrificando clientes."
      }
    }
  }
};
