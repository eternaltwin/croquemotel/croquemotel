export default {
  welcome: "Welcome",
  menu: {
    index: "Discover the game",
    play: "Play",
    visit: "Visit a random hotel",
    help: "Help",
    office: "Office",
    lab: "Lab",
    shop: "Supermarket",
    friends: "My friends"
  },
  hotel: {
    room: {
      none: {
        title: "Space available for a room",
        desc: "A nice empty space which you will doubtless fill pretty quickly...",
        info: "Use this available space to construct a *new room*."
      },
      bed: {
        title: "Bedroom",
        desc: "Guests can be so fussy sometimes: sometimes they even refuse to sleep on the floor in the corridor...",
        info: "Bedrooms can be used as *temporary residences for guests*."
      },
      lobby: {
        title: "Reception",
        desc: "A nice desk and key rack (imitation, of course) to look professional. ",
        info: "Position your employees at the welcome desk to accelerate the arrival of your next guests."
      },
      lobbySlot: {
        title: "An empty space suitable for another reception desk",
        desc: "For the time being, that seems to be where your guests are hanging out these days.",
        info: "You can build an additional *welcome desk* here."
      },
      restaurant: {
        title: "Astro-Gastro",
        desc: "There is nothing astronomical here except the price.",
        info: "Guests who appreciate their *food* can head there to dispose of their cash."
      },
      bin: {
        title: "Waste Disposal",
        desc: "The secret to this is that we chuck everything in here and never empty it.",
        info: "Guests who enjoy thoroughly *repulsive smells* can come here to shell out their hard-earned."
      },
      disco: {
        title: "Disco",
        desc: "Nobody has the slightest clue of what anyone else is saying, and everyone else is happy with that.",
        info: "Clients who appreciate *noise* can go here to spend their money."
      },
      furnace: {
        title: "Cremat-oven",
        desc: "It's like a self-cleaning oven, but with chairs inside.",
        info: "Clients who appreciate *heat* can go here to spend their money.."
      },
      pool: {
        title: "X-tra Wet Room",
        desc: "The water used in this attraction has successfully been made twice as wet by adding a ring of micro-droplets around each drop of water.",
        info: "Clients who appreciate *humidity* can go here to spend their money."
      },
      wash: {
        title: "Laundromatrix",
        desc: "A top-end washing machine which, aside from washing clothes, can sort colours, isolate and treat 54 varieties of stain, dry the items, iron the clothes and fold them neatly in a pile.",
        info: "Allows you to do guests' laundry if they so request."
      },
      shoe: {
        title: "Lustro-pump",
        desc: "A machine which can clean, polish and disinfect any pair of shoes.",
        info: "Allows you to clean guests' shoes should they require it."
      },
      alcool: {
        title: "Shambrewery",
        desc: "A distributor of alcoholic beverages consisting of massive vats of petrol and perfume linked together by a complex network of pipes. Officially speaking, this is called champagne.",
        info: "This allows you to give alcoholic drinks to guests in need of a massive hangover the following day."
      },
      fridge: {
        title: "Chowrightnow",
        desc: "The modern solution to an age-old problem: the empty fridge. Specially calibrated to satisfy the demands of monsters, in a matter of minutes it can deliver enough food to entertain guests for several years.",
        info: "Lets you restock the mini-fridge for any guests who are somewhat peckish."
      },
      lab: {
        title: "Institute of Happiness",
        desc: "The best way to better understand future guests is to study the current ones by cutting them into tiny pieces.",
        info: "Sacrificing guests allows you to earn *Research Points*."
      }
    }
  }
};
