import FontFaceObserver from "fontfaceobserver";
import { gsap } from "gsap";
import { CustomEase } from "gsap/CustomEase";
import { PixiPlugin } from "gsap/PixiPlugin";
import * as PIXI from "pixi.js";
import { Application, Container, DisplayObject, InteractionData, InteractionEvent, Loader, LoaderResource, Rectangle, SCALE_MODES, settings } from "pixi.js";

import { IHotelPlayer } from "§/contracts/hotel.interface";

import { Builder } from "./Builder";
import { GameContext, GameFonts, GameResources } from "./GameContext";

gsap.registerPlugin(CustomEase, PixiPlugin);
PixiPlugin.registerPIXI(PIXI);

type DraggingInteractionEvent = {
  currentTarget: DisplayObject & {
    data?: InteractionData,
    dragging: boolean,
    draggingDelta: {x: number, y: number}
  };
} & InteractionEvent;

type GameConstructor = {
  vue: Vue,
  view: HTMLCanvasElement,
  width: number,
  height: number,
  resources: GameConstructorResources,
  fonts: GameFonts,
  seed: string,
  hotel: IHotelPlayer
};

type GameConstructorResources = {
  spritesNearest: string,
  spritesLinear: string
};

export class Game {
  private app: Application;
  private ctx: GameContext;
  private builder!: Builder;
  private draggableStage?: Container;
  private initializationSteps = { fonts: false, resources: false };

  constructor({ vue, view, width, height, resources, fonts, seed, hotel }: GameConstructor) {
    settings.SCALE_MODE = SCALE_MODES.NEAREST;

    this.app = new Application({
      view,
      width,
      height,
      antialias: true,
      autoDensity: true,
      resolution: 1
    });

    this.ctx = new GameContext();

    this.ctx.setVue(vue);

    this.ctx.setScreen(this.app.screen);

    this.ctx.setSeed(seed);

    this.ctx.setData({ hotel });

    this.loadFonts(fonts);

    this.loadResources(resources);

    (window as any)._game = this;
    console.debug(seed);
  }

  private loadResources(resources: GameConstructorResources) {
    Object.entries(resources).forEach(r => this.app.loader.add(...r));

    this.app.loader.load((_loader: Loader, r: { [name: string]: LoaderResource }) => {
      const resources = r as GameResources;

      resources.spritesNearest.spritesheet!.baseTexture.scaleMode = SCALE_MODES.NEAREST;
      resources.spritesLinear.spritesheet!.baseTexture.scaleMode = SCALE_MODES.LINEAR;

      this.ctx.setResources(resources);
      this.initializationSteps.resources = true;
      this.onAssetsLoaded();
    });
  }

  private loadFonts(fonts: GameFonts) {
    const promises: Promise<void>[] = [];

    for (const fontFamilyName of Object.values(fonts)) {
      const fontLoader = new FontFaceObserver(fontFamilyName);
      promises.push(fontLoader.load());
    }

    Promise.all(promises).then(() => {
      this.ctx.setFonts(fonts);
      this.initializationSteps.fonts = true;
      this.onAssetsLoaded();
    });
  }

  private onAssetsLoaded() {
    if (!this.initializationSteps.fonts || !this.initializationSteps.resources) {
      return;
    }

    this.builder = new Builder(this.ctx);
    const buildResults = this.builder.buildAll();

    this.app.stage.addChild(buildResults.fixedBackground);

    this.draggableStage = new Container();
    this.draggableStage.interactive = true;
    this.draggableStage.hitArea = new Rectangle(0, 0, this.ctx.map.width, this.ctx.map.height);
    this.draggableStage
      .on("pointerdown", this.onDragStart.bind(this))
      .on("pointerup", this.onDragEnd.bind(this))
      .on("pointerupoutside", this.onDragEnd.bind(this))
      .on("pointermove", this.onDragMove.bind(this));
    this.app.stage.addChild(this.draggableStage);

    this.draggableStage.addChild(buildResults.hotelBackground);

    this.draggableStage.addChild(buildResults.hotel);

    this.draggableStage.x = -(this.ctx.map.width / 4);
    this.draggableStage.y = -(this.ctx.map.height - this.ctx.screen.height);
  }

  private onDragStart(event: DraggingInteractionEvent) {
    event.currentTarget.data = event.data;
    event.currentTarget.dragging = true;

    const eventPosition = event.data!.getLocalPosition(event.currentTarget.parent);
    event.currentTarget.draggingDelta = {
      x: eventPosition.x - event.currentTarget.x,
      y: eventPosition.y - event.currentTarget.y
    };
  }

  private onDragEnd(event: DraggingInteractionEvent) {
    event.currentTarget.data = undefined;
    event.currentTarget.dragging = false;
  }

  private onDragMove(event: DraggingInteractionEvent) {
    if (!event.currentTarget.dragging) {
      return;
    }

    const newPosition = event.currentTarget.data!.getLocalPosition(event.currentTarget.parent);
    event.currentTarget.x = Math.max(Math.min(0, newPosition.x - event.currentTarget.draggingDelta.x), -(this.ctx.map.width - this.ctx.screen.width));
    event.currentTarget.y = Math.max(Math.min(0, newPosition.y - event.currentTarget.draggingDelta.y), -(this.ctx.map.height - this.ctx.screen.height));
  }
}
