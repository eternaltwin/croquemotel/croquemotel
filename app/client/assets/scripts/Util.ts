export class Util {
  /**
   * Make a color lighten (positive magnitude) or darker (negative magnitude).
   * @param color The original color to change.
   * @param magnitude The positive or negative magnitude to apply.
   * @returns A new color, lighten or darker.
   * @see https://natclark.com/tutorials/javascript-lighten-darken-hex-color/
   */
  public static newColorShade(color: number, magnitude: number): number {
    let r = (color >> 16) + magnitude;
    r > 255 && (r = 255);
    r < 0 && (r = 0);
    let g = (color & 0x0000FF) + magnitude;
    g > 255 && (g = 255);
    g < 0 && (g = 0);
    let b = ((color >> 8) & 0x00FF) + magnitude;
    b > 255 && (b = 255);
    b < 0 && (b = 0);
    return g | (b << 8) | (r << 16);
  }

  /**
   * Returns a pseudorandom number between 0 and `max`
   * @param max The maximum random value (included).
   * @returns A random floating point number between 0 and `max`.
   */
  public static random(max: number): number;
  /**
   * Returns a pseudorandom number between `min` and `max`
   * @param min The minimum random value (included).
   * @param max The maximum random value (included).
   * @returns A random floating point number between `min` and `max`.
   */
  public static random(min: number, max: number): number;

  public static random(min: number, max?: number): number {
    if (max == null) {
      max = min; min = 0;
    }
    if (min > max) {
      const tmp = min; min = max; max = tmp;
    }
    return min + (max - min) * Math.random();
  }

  public static pad(value: any, length: number): string {
    return (value.toString().length < length) ? Util.pad("0" + value, length) : value;
  }
}
