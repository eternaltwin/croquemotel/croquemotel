import { DropShadowFilter } from "@pixi/filter-drop-shadow";
import { LoaderResource, Rectangle, Resource, Texture } from "pixi.js";
import { IVueI18n } from "vue-i18n";

import { IHotelPlayer } from "§/contracts/hotel.interface";

type MapData = {
  width: number,
  height: number,
  ground: number
};

type GameData = {
  hotel: IHotelPlayer
};

export type GameFonts = {
  default: string,
  hotelTitle: string,
  hotelRoomTitle: string,
  hotelClock: string
};

export type GameResources = {
  spritesNearest: LoaderResource,
  spritesLinear: LoaderResource
};

export class GameContext {
  private _vue?: Vue = undefined;
  private _resources?: GameResources = undefined;
  private _sprites?: { [name: string]: Texture<Resource> } = undefined;
  private _spritesAnimations?: { [name: string]: Texture<Resource>[] } = undefined;
  private _fonts?: GameFonts = undefined;
  private _map?: MapData = undefined;
  private _seed?: number[] = undefined;
  private _data?: GameData = undefined;
  private _screen?: Rectangle = undefined;
  private _shadowFilter?: DropShadowFilter = undefined;

  public get i18n(): IVueI18n {
    return this._vue!.$i18n;
  }

  public get resources(): GameResources {
    return this._resources!;
  }

  public get sprites(): { [name: string]: Texture<Resource> } {
    return this._sprites!;
  }

  public get spritesAnimations(): { [name: string]: Texture<Resource>[] } {
    return this._spritesAnimations!;
  }

  public get fonts(): GameFonts {
    return this._fonts!;
  }

  public get map(): MapData {
    return this._map!;
  }

  public get seed(): number[] {
    return this._seed!;
  }

  public get data(): GameData {
    return this._data!;
  }

  public get screen(): Rectangle {
    return this._screen!;
  }

  public get shadowFilter(): DropShadowFilter {
    // eslint-disable-next-line no-return-assign
    return this._shadowFilter ??= new DropShadowFilter({
      rotation: 0,
      blur: 0,
      pixelSize: 0,
      distance: 8,
      color: 0x152565,
      alpha: 0.6
    });
  }

  public setVue(value: Vue) {
    this._vue = value;
  }

  public setResources(value: GameResources) {
    this._resources = value;
    this._sprites = Object.assign({}, value.spritesNearest.textures!, value.spritesLinear.textures!);
    this._spritesAnimations = Object.assign({}, value.spritesNearest.spritesheet!.animations, value.spritesLinear.spritesheet!.animations);
  }

  public setFonts(value: GameFonts) {
    this._fonts = value;
  }

  public setMap({ width, height }: { width: number, height: number }) {
    this._map = {
      width,
      height,
      ground: height - this.sprites.ground.height ?? 0
    };
  }

  public setSeed(value: number[] | string) {
    this._seed = typeof value === "string"
      ? Buffer.from(value, "base64").toString().split(";").map(s => parseFloat(s))
      : value;
  }

  public setData(value: GameData) {
    this._data = value;
  }

  public setScreen(value: Rectangle) {
    this._screen = value;
  }

  /**
   * Generates a floating point pseudo-random number.
   * @param x First param.
   * @returns A floating point, pseudo-random number.
   */
  public noise(x: number): number;
  /**
   * Generates a floating point pseudo-random number.
   * @param x First param.
   * @param y Second param.
   * @returns A floating point, pseudo-random number.
   */
  public noise(x: number, y: number): number;
  /**
   * Generates a floating point pseudo-random number (between `min` and `max`).
   * @param x First param.
   * @param min Minimum value (included).
   * @param max Maximum value (included).
   * @returns A floating point, pseudo-random number in the range `min` to `max`.
   */
  public noise(x: number, min: number, max: number): number;
  /**
   * Generates a floating point pseudo-random number (between `min` and `max`).
   * @param x First param.
   * @param y Second param.
   * @param min Minimum value (included).
   * @param max Maximum value (included).
   * @returns A floating point, pseudo-random number in the range `min` to `max`.
   */
  public noise(x: number, y: number, min: number, max: number): number;

  public noise(x: number, y?: number, min?: number, max?: number): number {
    const ceil = 0xFF;

    if (typeof y === "undefined") {
      y = 0;
      min = 0;
      max = 1;
    } else if (typeof min === "undefined") {
      min = 0;
      max = 1;
    } else if (typeof max === "undefined") {
      max = min;
      min = y;
      y = 0;
    }

    const result = (Math.sin((x * this.seed[0] + y! * this.seed[1]) * this.seed[2]) * 1000000) & ceil;
    return min! + (result * (max! - min!)) / ceil;
  }

  /**
   * Generates an integer pseudo-random number.
   * @param x First param.
   * @returns An integer, pseudo-random number.
   */
  public noiseInt(x: number): number;
  /**
   * Generates an integer pseudo-random number.
   * @param x First param.
   * @param y Second param.
   * @returns An integer, pseudo-random number.
   */
  public noiseInt(x: number, y: number): number;
  /**
   * Generates an integer pseudo-random number (between `min` and `max`).
   * @param x First param.
   * @param min Minimum value (included).
   * @param max Maximum value (included).
   * @returns An integer, pseudo-random number in the range `min` to `max`.
   */
  public noiseInt(x: number, min: number, max: number): number;
  /**
   * Generates an integer pseudo-random number (between `min` and `max`).
   * @param x First param.
   * @param y Second param.
   * @param min Minimum value (included).
   * @param max Maximum value (included).
   * @returns An integer, pseudo-random number in the range `min` to `max`.
   */
  public noiseInt(x: number, y: number, min: number, max: number): number;

  public noiseInt(x: number, y?: number, min?: number, max?: number): number {
    if (typeof y === "undefined") {
      y = 0;
      min = 0;
      max = 1;
    } else if (typeof min === "undefined") {
      min = 0;
      max = 1;
    } else if (typeof max === "undefined") {
      max = min;
      min = y;
      y = 0;
    }

    return Math.round(this.noise(x, y!, Math.ceil(min!), Math.floor(max!)));
  }

  /**
   * Generates a bit pseudo-random number.
   * @param x First param.
   * @returns An bit, pseudo-random number.
   */
  public noiseBit(x: number): number;
  /**
   * Generates a bit pseudo-random number.
   * @param x First param.
   * @param y Second param.
   * @returns An bit, pseudo-random number.
   */
  public noiseBit(x: number, y: number): number;

  public noiseBit(x: number, y?: number): number {
    return this.noiseInt(x, y ?? 0, 0, 1);
  }

  /**
   * Generates a signed bit (-1 or 1) pseudo-random number.
   * @param x First param.
   * @returns An bit, pseudo-random number.
   */
  public noiseSBit(x: number): number;
  /**
   * Generates a signed bit (-1 or 1) pseudo-random number.
   * @param x First param.
   * @param y Second param.
   * @returns An bit, pseudo-random number.
   */
  public noiseSBit(x: number, y: number): number;

  public noiseSBit(x: number, y?: number): number {
    return (this.noiseInt(x, y ?? 0, -1, 0) * 2) + 1;
  }

  /**
   * Localize the message of `key` using the default locale.
   * @param key The key to localize.
   * @param values Value to inject in translation.
   */
  public t(key: string, values?: any[] | { [key: string]: any }): string;
  /**
   * Localize the message of `key` using `locale`.
   * @param key The key to localize.
   * @param locale The locale to use.
   * @param values Value to inject in translation.
   */
  public t(key: string, locale: string, values?: any[] | { [key: string]: any }): string;

  public t(...args: any[]): string {
    // eslint-disable-next-line prefer-spread
    return this._vue!.$i18n.t.apply(this._vue!.$i18n, args as any).toString();
  }

  /**
   * Localize the message of `key` with pluralization using the default locale.
   * @param key The key to localize.
   * @param choice Default 1.
   * @param values Value to inject in translation.
   */
  public tc(key: string, choice?: number, values?: any[] | { [key: string]: any }): string;
  /**
   * Localize the message of `key` with pluralization using `locale`.
   * @param key The key to localize.
   * @param choice Default 1.
   * @param locale The locale to use.
   * @param values Value to inject in translation.
   */
  public tc(key: string, choice: number, locale: string, values?: any[] | { [key: string]: any }): string;

  public tc(...args: any[]): string {
    // eslint-disable-next-line prefer-spread
    return this._vue!.$i18n.tc.apply(this._vue!.$i18n, args as any).toString();
  }
}
