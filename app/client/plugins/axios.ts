import { Plugin } from "@nuxt/types";

import { authStore } from "~/store";

const axiosPlugin: Plugin = ({ $axios, redirect }) => {
  $axios.onError((error) => {
    if (error.response?.status === 401) {
      authStore.clear();
      redirect("/login");
    }
  });
};

export default axiosPlugin;
